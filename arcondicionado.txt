� muito interessante! O manual do meu carro diz para abaixar as janelas para que todo o ar quente saia antes de ligar o ar condicionado. POR QUE?

N�o � de admirar que tem mais gente morrendo de c�ncer do que nunca. Queria saber de onde isso vem, mas aqui est� um exemplo que explica muito dos incidentes que t�m causado c�ncer.

Muitas pessoas est�o nos seus carros a come�ar pela manh�, e por �ltimo a noite, 7 dias por semana.

Ler isso me faz sentir culpado e doente. Por favor passe essa informa��o para quantas pessoas for poss�vel. Nunca � tarde demais para fazer algumas mudan�as!

Por favor, N�O ligue o ar condicionado assim que entrar no carro.

Abra as janelas ao entrar no carro e depois de alguns minutos, LIGUE o ar condicionado.

Aqui est� o motivo: de acordo com pesquisas, o painel do carro, os assentos, dutos do ar condicionado, na verdade todos os objetos de pl�stico em seu ve�culo emitem Benzeno, uma toxina que causa c�ncer. Uma subst�ncia MUITO cancer�gena. Separe um tempo para observar o cheiro de pl�stico aquecido em seu carro quando voc� abri-lo, e antes de ligar o motor.

Al�m de causar c�ncer, Benzeno intoxica os ossos, causa anemia e reduz as c�lulas brancas do sangue. Uma exposi��o prolongada pode causar leucemia e aumenta o risco de alguns tipos de c�ncer. Ele tamb�m pode causar aborto em mulheres gr�vidas.

O n�vel "aceit�vel" de Benzeno em lugares fechados � de 50mg por p� quadrado (144 polegadas quadradas, medida utilizada nos estados unidos).

Um carro estacionado em lugar fechado, com as janelas fechadas, ir� conter de 400 a 800 mg de Benzeno - 8 vezes mais que o aceit�vel.

Se estacionado em lugares abertos ao Sol, numa temperatura de 16�C (converti, o texto dizia 60�F), o n�vel de benzeno vai para 2000 a 4000 mg, 40 vezes a mais que o aceit�vel.

Quem entra no carro mantendo as janelas fechadas, vai inalar quantidades excessivas de toxina Benzeno.

O Benzeno � uma toxina que afeta seus rins e f�gado. E o pior, � extremamente dif�cil para o seu corpo para expulsar esse material t�xico.

Ent�o amigos, por favor abram as janelas e portas do seu carro, d� um tempo para arejar o interior (dissipar o conte�do mortal) antes de entrar no ve�culo.